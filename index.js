Vue.prototype.$http = axios

var app = new Vue({
    el: '#app',
    data: {
        title: "Cadavre Exquis",
        phrase: []
    },
    methods: {
        async generate() {
            // Mon register
            const registerUrl = 'https://app-f343a079-66ae-43b0-b562-4678c9dbca08.cleverapps.io'
            // register spécial central
            // const registerUrl = 'https://app-aee3dbf0-7936-4560-be94-5b1484dfd0c2.cleverapps.io'

            let verbUrl = ''
            let subjectUrl = ''
            let adjectiveUrl = ''

            await this.$http(`${registerUrl}/providers?type=VERB`)
                .then(res => {
                    const index = Math.floor(Math.random() * res.data.length)
                    verbUrl = res.data[index] && res.data[index].url
                })
                .catch(err => console.log(err))

            await this.$http(`${registerUrl}/providers?type=SUBJECT`)
                .then(res => {
                    const index = Math.floor(Math.random() * res.data.length)
                    subjectUrl = res.data[index] && res.data[index].url
                })
                .catch(err => console.log(err))

            await this.$http(`${registerUrl}/providers?type=ADJECTIVE`)
                .then(res => {
                    const index = Math.floor(Math.random() * res.data.length)
                    adjectiveUrl = res.data[index] && res.data[index].url
                })
                .catch(err => console.log(err))

            let vm = this

            Promise.all([
                adjectiveUrl? this.$http(`${adjectiveUrl}/adjective`) : { data: { value: 'Error', instanceId: null } },
                subjectUrl? this.$http(`${subjectUrl}/subject`) : { data: { value: 'Error', instanceId: null } },
                verbUrl? this.$http(`${verbUrl}/verb`) : { data: { value: 'Error', instanceId: null } },
                adjectiveUrl? this.$http(`${adjectiveUrl}/adjective`) : { data: { value: 'Error', instanceId: null } },
                subjectUrl? this.$http(`${subjectUrl}/subject`) : { data: { value: 'Error', instanceId: null } }
            ]).then((res) => {
                res = res.map(obj => {
                    let outPut = {}
                    outPut.value = obj.data.value
                    outPut.instanceId = obj.data.instanceId
                    return outPut
                })
                this.phrase = res
            })
        }
    }
})